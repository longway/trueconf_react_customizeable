import React, { useState, useEffect, createRef } from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView
} from 'react-native'
import TrueConfWrapper from 'react-native-trueconf-react-sdk'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
// setTimeout(() => {
//   TrueConfView.initTC()
// }, 2000)
// console.log('TrueConfView', TrueConfView)
// function testTC() {
//   setTimeout(() => {
//     console.log('init')
//     TrueConfView.initTC()
//
//     setTimeout(() => {
//       console.log('hide')
//       TrueConfView.hideTC()
//       testTC()
//     }, 2000)
//
//   }, 2000)
// }
// testTC()

const greyColor = '#bed1c0'

export default function App () {
  const [isCounterShowed, setIsCounterShowed] = useState(true)
  const [isConnectedToServer, setIsConnectedToServer] = useState(false)
  const [isLoggedIn, setIsLoggedIn] = useState(false)
  const [isCalling, setIsCalling] = useState(false)
  const [isLoggingIn, setIsLoggingIn] = useState(false)
  const [muted, setMuted] = useState(false)
  const [cameraOn, setCameraOn] = useState(true)

  const counterRef = createRef()

  // useEffect(() => {
  //   console.log('counterRef', counterRef, counterRef.current.testMethod)
  //
  //   setTimeout(() => {
  //     setMuted(true)
  //
  //     // setTimeout(() => {
  //     //   setIsCounterShowed(true)
  //     // }, 2000)
  //
  //   }, 2000)
  // }, [])

  function onServerStatus (e) {
    console.log('onServerStatus', e)
  }

  function onStateChanged (e) {
    console.log('onStateChanged', e, isLoggedIn, isConnectedToServer)
    // { isLoggedIn, isConnectedToServer }
    if (e.isConnectedToServer !== isConnectedToServer) {
      console.log('onStateChanged-1')
      setIsConnectedToServer(e.isConnectedToServer)
    }

    if (!e.isConnectedToServer) return
    if (e.isLoggedIn !== isLoggedIn) {
      console.log('onStateChanged-2')
      setIsLoggedIn(e.isLoggedIn)
    }

    if (e.isLoggedIn) return
    if (e.isLoggingIn) return
    setIsLoggingIn(true)
    console.log('login')

    counterRef.current.login({
      userId: 'test1_SDK',
      password: 'test1_SDK',
      encryptPassword: true,
      enableAutoLogin: true
    })
  }

  function onLogin ({ isLoggedIn, userID }) {
    console.log('onLogin', isLoggedIn, userID)
    setIsLoggedIn(isLoggedIn)
  }

  function callOrHangup () {
    console.log('callOrHangup')
    if (!isCalling) {
      makeCall()
      setIsCalling(true)
      return
    }

    hangup()
    setIsCalling(false)
  }

  function makeCall () {
    console.log('makeCall')
    counterRef.current.makeCall()
  }

  function hangup () {
    console.log('hangup')
    counterRef.current.hangup()
  }

  function toggleMute () {
    setMuted(!muted)
  }

  function toggleCameraOn () {
    setCameraOn(!cameraOn)
  }

  const isActive = isConnectedToServer && isLoggedIn

  return (
    <View style={{ flex: 1, backgroundColor: '#b07878' }}>
      {isCounterShowed &&
        <TrueConfWrapper
          ref={counterRef}
          onStateChanged={onStateChanged}
          onServerStatus={onServerStatus}
          onLogin={onLogin}
          muted={muted}
          cameraOn={cameraOn}
          style={{ flex: 1 }}
        />}
      <SafeAreaView style={{ position: 'absolute', left: 0, bottom: 0, flexDirection: 'column', backgroundColor: 'rgba(0,0,0,.1)' }}>
        <Text style={{ padding: 10, color: '#fff' }}>
          { `Подключено к серверу: ${isConnectedToServer ? 'Да' : 'Нет'}` }
        </Text>
        <Text style={{ padding: 10, color: '#fff' }}>
          { `Авторизованы: ${isLoggedIn ? 'Да' : 'Нет'}` }
        </Text>
        <View style={{ flexDirection: 'row', alignItems: 'center', margin: 10 }}>
          <TouchableOpacity
            style={{ width: 50, height: 50 }}
            onPress={callOrHangup}
            activeOpacity={isActive ? 0.4 : 1}
          >
            <View
              style={{
                backgroundColor: isActive ? (isCalling ? '#ec6a6a' : '#6aec77') : greyColor,
                flex: 1,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <MaterialIcons name='phone' size={30} color={'#fff'} style={{ lineHeight: 30 }} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ width: 50, height: 50, marginLeft: 25 }}
            onPress={toggleMute}
            activeOpacity={isActive ? 0.4 : 1}
          >
            <View
              style={{
                backgroundColor: isActive ? '#6ac5ec' : greyColor,
                flex: 1,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <MaterialIcons name={muted ? 'volume-mute' : 'volume-up'} size={30} color={'#fff'} style={{ lineHeight: 30 }} />
            </View>
          </TouchableOpacity>
          <TouchableOpacity
            style={{ width: 50, height: 50, marginLeft: 25 }}
            onPress={toggleCameraOn}
            activeOpacity={isActive ? 0.4 : 1}
          >
            <View
              style={{
                backgroundColor: isActive ? '#6ac5ec' : greyColor,
                flex: 1,
                borderRadius: 25,
                justifyContent: 'center',
                alignItems: 'center'
              }}
            >
              <MaterialIcons name={cameraOn ? 'videocam' : 'videocam-off'} size={30} color={'#fff'} style={{ lineHeight: 30 }} />
            </View>
          </TouchableOpacity>
        </View>
      </SafeAreaView>
    </View>
  )
}
