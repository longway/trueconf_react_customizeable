//
//  TCTheme.h
//  SDKLibrary
//
//  Copyright © 2018 TrueConf. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    TCThemeUID_LightBlue, // has string name
    TCThemeUID_LightRed,  // has string name
    TCThemeUID_BlackBlue, // has string name
    TCThemeUID_Server,    // downloaded from server
    TCThemeUID_None
} TCThemeUID; // theme unique IDs - their order should not change in future

@interface TCTheme : NSObject

@property (nonatomic) TCThemeUID uid; // theme unique ID - it should not change in future
@property (nonatomic) long unsigned index; // theme index
@property (nonatomic) long unsigned indexi; // index of -theme-X suffix for images
@property (nonatomic) UIColor* color; // general scheme color - blue, red, black
@property (nonatomic) UIColor* tintColor; // tint color - blue, red, blue for black scheme

@property (nonatomic) UIColor* btnTextColor; // buttons text color
@property (nonatomic) UIColor* btnTextDisabledColor; // buttons text color in disabled state
@property (nonatomic) UIColor* btnTextHighlightedColor; // buttons text color in pressed state
@property (nonatomic) UIColor* disabledControlColor; // color for disabled controls (gray for black scheme for example)
@property (nonatomic) UIColor* panelBGColor; // panels color - light blue, light red, black
@property (nonatomic) UIColor* infoBGColor; // info panel color - light blue, light red, dark gray
@property (nonatomic) UIColor* infoBorderColor; // info panel border color - white, gray
@property (nonatomic) UIColor* ccBGColor; // conf controls panel color - white, dark gray
@property (nonatomic) UIColor* ccBorderColor; // conf controls panel border color - gray, gray
@property (nonatomic) UIColor* btnSelectedRedColor; // color for selected list button (End, Lstatus)
@property (nonatomic) UIColor* btnSelectedRedHighlightedColor; // color in pressed state
@property (nonatomic) UIColor* listArrowColor; // color for arrow in list/table
@property (nonatomic) UIColor* boxBGColor; // color for some lists/tables - white, dark gray
@property (nonatomic) UIColor* blackTextColor; // black text in light schemes, white text in dark schemes
@property (nonatomic) UIColor* grayTextColor; // gray text in light schemes, another gray text in dark schemes
@property (nonatomic) UIColor* tableBGColor; // table background (contacts, chats) - white, black
@property (nonatomic) UIColor* tableCellHighlightColor; // highlighted cell in table - light gray, dark gray
@property (nonatomic) UIColor* tableSeparatorColor; // table separator color - light gray, dark gray
@property (nonatomic) UIBarStyle searchBarStyle; // light or dark search bar style
@property (nonatomic) UIStatusBarStyle statusBarStyle; // light or dark status bar style
@property (nonatomic) UIColor* tableGroupBGColor; // color for cells if groups is ON - white, dark gray
@property (nonatomic) UIColor* editFieldBGColor; // color for Edit field background - white, dark gray
@property (nonatomic) UIColor* editFieldHintColor; // color for Edit field's Hin - light gray, darker gray
@property (nonatomic) UIKeyboardAppearance keyboardAppearance; // light or dark
@property (nonatomic) UIColor* conferenceBGColor; // background color for conference view - white, black
@property (nonatomic) BOOL windowBorder;
@property (nonatomic) UIColor* windowBorderColor;
@property (nonatomic) UIColor* tableCellOwnerBGColor; // conference owner's table cell BG color - light yellow
@property (nonatomic) UIBlurEffectStyle blurEffectStyle;
@property (nonatomic) UIColor* groupBGColor; // color behind the groups - black
@property (nonatomic) UIColor* groupBGiPadColor; // color behind the groups on iPad - gray
@property (nonatomic) UIColor* inviteBtnForGroupiPadColor; // invite friends - button in the bottom of the contacts list. Usually it is of Button color. But in some cases (light theme on iPad) it could be white or black
@property (nonatomic) UIColor* avatarPlaceholderColor; // view BG color - light gray, dark gray
@property (nonatomic) UIColor* avatarPlaceholderTextBGColor; // text BG color - (darker) light gray, (lighter) dark gray
@property (nonatomic) UIColor* avatarPlaceholderTextBGImgColor; // text BG color (with image) - gray
@property (nonatomic) UIColor* avatarPlaceholderTextColor; // text color - white
@property (nonatomic) UIColor* avatarPlaceholderTextImgColor; // text color (with image) - white

@property (nonatomic) NSString* localizedName; // creative localizable name

// properties for downloaded themes

@property (nonatomic) BOOL defaultTheme; // use as default (if there are several themes from server)
@property (nonatomic) NSDate* imagesDate;
@property (nonatomic) NSDate* imagesDateSaved; // read from NSUserDefaults
@property (nonatomic) NSDictionary* strings;
@property (nonatomic) NSURL* imagesFile;
@property (nonatomic) NSMutableArray<NSString*>* imagesFiles;
@property (nonatomic) NSString* icon;

- (instancetype)copy;
- (UIImage*) imageNamed:(NSString*)img;
- (UIImage*) imageNamed:(NSString*)img inBundle:(nullable NSBundle *)bundle;
- (NSString* _Nullable) text:(NSString* _Nullable) txt;

@end
