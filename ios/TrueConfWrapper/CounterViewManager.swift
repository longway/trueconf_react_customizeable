//
//  CounterViewManager.swift
//  trueconf_react_customizeable
//
//  Created by kesha on 29/08/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

@objc(CounterViewManager)
class CounterViewManager: RCTViewManager {
  var counterView: CounterView?
  
  override func view() -> UIView! {    
    counterView = CounterView()
    counterView!.initViewsAndSdk()
    return counterView
  }
  
  @objc
  func testMethod() {
    
  }
  
  @objc
  func makeCallFromManager(_ node: NSNumber, to: NSString) {
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
      ) as! CounterView
      component.makeCall(to: to)
    }
  }
  
  @objc
  func hangup(_ node: NSNumber) {
    DispatchQueue.main.async {
      let component = self.bridge.uiManager.view(
        forReactTag: node
      ) as! CounterView
      component.hangup()
    }
  }
  
  override static func requiresMainQueueSetup() -> Bool {
     return true
   }
  
}
