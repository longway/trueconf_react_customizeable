//
//  CounterView.swift
//  trueconf_react_customizeable
//
//  Created by kesha on 29/08/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import UIKit

class CounterView : UIView, UITextFieldDelegate, TCConfControlsDelegate, TCWindowsDelegate {
  var tcsdk: TCSDK?
  var xview: UIView?
  var xsview: UIView?
  
  @objc var onServerStatus: RCTDirectEventBlock?
  @objc var onStateChanged: RCTDirectEventBlock?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
      super.init(coder: aDecoder)
  }
  
  func initViewsAndSdk() {
    self.xview = UIView()
    self.addSubview(self.xview!)
    self.xview!.backgroundColor = .brown
    
    self.xview!.translatesAutoresizingMaskIntoConstraints = false
    self.xview!.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
    self.xview!.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
    self.xview!.widthAnchor.constraint(equalTo: self.widthAnchor).isActive = true
    self.xview!.heightAnchor.constraint(equalTo: self.heightAnchor).isActive = true
    
    self.xsview = UIView()
    self.xview!.addSubview(self.xsview!)
    self.xsview!.backgroundColor = .blue
    
    self.xsview!.translatesAutoresizingMaskIntoConstraints = false
    self.xsview!.rightAnchor.constraint(equalTo: self.xview!.safeRightAnchor).isActive = true
    self.xsview!.bottomAnchor.constraint(equalTo: self.xview!.safeBottomAnchor).isActive = true
    let xsview_width = UIScreen.main.bounds.width / 3
    self.xsview!.widthAnchor.constraint(equalToConstant: xsview_width).isActive = true
    self.xsview!.heightAnchor.constraint(equalToConstant: xsview_width * 2).isActive = true
   
    let activityIndicatorView = UIView()
    self.xview!.addSubview(activityIndicatorView)
    activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
    activityIndicatorView.centerXAnchor.constraint(equalTo: self.xview!.centerXAnchor).isActive = true
    activityIndicatorView.centerYAnchor.constraint(equalTo: self.xview!.centerYAnchor).isActive = true
//    TODO: MOVE TO LEFT/TOP BY -23
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    activityIndicator.frame = CGRect(x: 0, y: 0, width: 46, height: 46)
    activityIndicator.startAnimating()
    activityIndicatorView.addSubview(activityIndicator)
    
    self.initSdk()
  }
  
  func initSdk() {
    if (self.tcsdk != nil ) { return }
    
    DispatchQueue.main.async {
      guard let rootViewController = UIApplication.shared.delegate?.window??.rootViewController else { return }
      self.tcsdk = TCSDK(viewController: rootViewController, forServer: "ru10.trueconf.net", confCustomControlsImages: nil)
      
      self.tcsdk!.confControlsDelegate = self
      self.tcsdk!.windowsDelegate = self
      self.tcsdk!.xview = self.xview!
      self.tcsdk!.xsview = self.xsview!
      
      
      self.tcsdk!.onServerStatus( { (connected: Bool, serverName: String, serverPort: Int) -> () in
        print("connected: " + String(connected))
        print("serverName: " + serverName)
        print("serverPort: " + String(serverPort))
        self.notifyOnServerStatus(connected: connected, serverName: serverName, serverPort: serverPort)
      } as? (Bool, String?, Int) -> Void  )
//      <#T##_onServerStatus: ((Bool, String?, Int) -> Void)?##((Bool, String?, Int) -> Void)?##(Bool, String?, Int) -> Void#>
//      self.tcsdk!.onServerStatus({ (connected: Bool, serverName: String, serverPort: Int) in
//        print("connected: " + String(connected))
//        print("serverName: " + serverName)
//        print("serverPort: " + String(serverPort))
////        slf.serverdata = connected ? "Подключены к серверу \(serverName):\(CLong(serverPort))" : "Не подключены к серверу"
////        slf.checkStatus()
//        } as? (Bool, String?, Int) -> Void)
      
      var loginCalled = false
      var isCalling = false

      self.tcsdk!.onStateChanged( { () -> () in
        print("onStateChanged")
        let isConnectedToServer = self.tcsdk!.isConnectedToServer()
        print("onStateChanged isConnectedToServer: " + String(isConnectedToServer))
        
        let isLoggedIn = self.tcsdk!.isLoggedIn()
        print("onStateChanged isLoggedIn: \(isLoggedIn)")
        
        self.notifyOnStateChanged(isConnectedToServer: isConnectedToServer, isLoggedIn: isLoggedIn)
        
        if (isConnectedToServer && !isLoggedIn && !loginCalled) {
          loginCalled = true
          self.tcsdk!.login(as: "test1_SDK", password: "test1_SDK", encryptPassword: true, enableAutoLogin: true)
        }
        
        if (isConnectedToServer && isLoggedIn && !isCalling) {
          isCalling = true
          print("Making a call")
//          self.makeCall()
        }
      } )
      
      self.tcsdk!.onLogin({ (loggedIn: Bool, userID: String) in
        print("onLogin isLoggedIn: " + String(loggedIn))
        print("onLogin userID: " + String(userID))
      } as? (Bool, String?) -> Void)
      
      self.tcsdk!.start()
    }
  }
  
  @objc
  func notifyOnServerStatus(connected: Bool, serverName: String, serverPort: Int) {
    print("notifyOnServerStatus")
    onServerStatus!(["connected": connected])
  }
  
  @objc
  func notifyOnStateChanged(isConnectedToServer: Bool, isLoggedIn: Bool) {
    print("notifyOnStateChanged")
    onStateChanged?([
      "isConnectedToServer": isConnectedToServer,
      "isLoggedIn": isLoggedIn
    ])
  }
  
  @objc
  func makeCall(to: NSString) {
    self.tcsdk!.call(to: String(to))
  }
  
  @objc
  func hangup() {
     self.tcsdk!.hangup()
   }

}


extension UIView {

  var safeTopAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.topAnchor
    }
    return self.topAnchor
  }

  var safeLeftAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *){
      return self.safeAreaLayoutGuide.leftAnchor
    }
    return self.leftAnchor
  }

  var safeRightAnchor: NSLayoutXAxisAnchor {
    if #available(iOS 11.0, *){
      return self.safeAreaLayoutGuide.rightAnchor
    }
    return self.rightAnchor
  }

  var safeBottomAnchor: NSLayoutYAxisAnchor {
    if #available(iOS 11.0, *) {
      return self.safeAreaLayoutGuide.bottomAnchor
    }
    return self.bottomAnchor
  }
}
