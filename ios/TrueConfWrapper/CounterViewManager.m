//
//  CounterViewManager.m
//  trueconf_react_customizeable
//
//  Created by kesha on 29/08/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import "React/RCTViewManager.h"

@interface RCT_EXTERN_MODULE(CounterViewManager, RCTViewManager)

RCT_EXPORT_VIEW_PROPERTY(server, NSString)
RCT_EXPORT_VIEW_PROPERTY(onServerStatus, RCTDirectEventBlock)
RCT_EXPORT_VIEW_PROPERTY(onStateChanged, RCTDirectEventBlock)

RCT_EXTERN_METHOD(testMethod)
RCT_EXTERN_METHOD(
  makeCallFromManager:(nonnull NSNumber *)node
  to:(nonnull NSString *)to
)
RCT_EXTERN_METHOD(
  hangup:(nonnull NSNumber *)node
)

@end
